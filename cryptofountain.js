require('dotenv').config();
var express = require('express');
var app = express();
var server = require('http').Server(app);
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var io = require('socket.io')(server);
var request = require('request');
var session = require('express-session');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var saltRounds = 10;
var BlockIo = require('block_io');
var block_io = new BlockIo(process.env.BLOCKIO_KEY, process.env.BLOCKIO_PIN, 2);

server.listen(8081, function () {
	console.log('Example app listening on port 3000!');
});

app.use(express.static(__dirname + '/public'));
app.use(session({
	secret: 'sup',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

function DbConnection() {
	return new Promise((resolve, reject) => {
		MongoClient.connect("mongodb://" + process.env.DB_USER + ":" + process.env.DB_PASSWORD + "@ds147080.mlab.com:47080/cryptofountain", 
			(err, db) => {
 				err ? reject(err) : resolve(db);
			}
		);
	});
};

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	else
		console.log('not authenticated');
		res.redirect('/');
};

// find distance between two points, haversine formula
function distance(lat1, lon1, lat2, lon2) {
	var p = 0.017453292519943295;    // Math.PI / 180
	var c = Math.cos;
	var a = 0.5 - c((lat2 - lat1) * p)/2 + 
		c(lat1 * p) * c(lat2 * p) * 
		(1 - c((lon2 - lon1) * p))/2;

	return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
};

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');	
});

passport.use('register', 
	new LocalStrategy(
		function (username, password, done) {
			bcrypt.hash(password, saltRounds, function (err, hash) {
				block_io.get_new_address({}, function (error, response){
					DbConnection().then(db => {
						db.collection('users').insert({
							'username': username,
							'password': hash,
							'faucets': [],
							'address': response.data.address,
							'addressLabel': response.data.label,
							'balance': 0
						}, function(err, user) {
							return done(null, user);
						});
					})
				});
			});
		}
	)
);

passport.use('login',
	new LocalStrategy(
		function (username, password, done) {
			DbConnection().then(db => {
			    db.collection('users').findOne({ 'username': username }, function (err, user) {
					if (err) { return done(err); }
					if (!user) { return done(null, false); }
					bcrypt.compare(password, user.password, function(err, res) {
						if (res = false) { return done(null, false); }
						return done(null, user);
					});
					// if (user.password != password) { return done(null, false); }
					// return done(null, user);
			    });
		  	});
		}
	)
);

passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (user, done) {
	done(null, user);
});

app.post('/register', 
	passport.authenticate('register'), 
	function (req, res) {
		console.log(req.session);
		res.redirect('/');
	}
);

app.post('/login', 
	passport.authenticate('login'), 
	function (req, res) {
		console.log(req.session);
		res.redirect('/');
	}
);

app.post('/logout', 
	function (req, res) {
		console.log(req.session);
		req.logout();
		console.log(req.session);
		res.redirect('/');
	}
);

app.post('/addfaucet', 
	isAuthenticated, 
	function (req, res) {
		DbConnection().then(db => {
			db.collection('fountains').insert({
				'user': req.user,
				'lat': req.body.lat,
				'lng': req.body.lng,
				'payRate': req.body.payRate,
				'faucetTitle': req.body.faucetTitle
			})
		})
		res.redirect('/');
	}
);

app.post('/checkfountains',
	function (req, res) {
		DbConnection().then(db => {
			db.collection('fountains').find().toArray(function(err, array) {
				for (i=0; i<array.length; i++) {
					theDistance = distance(req.body.lat, req.body.lng, array[i].lat, array[i].lng);
					if(theDistance < 0.1) {
						console.log(array[i]);
						return res.send(
							'<div>You are in range of a fountain. Enter your bitcoin or email address to receive free bitcoin</div>' +
							'<form id="sendBTC" action="/sendBTC" method="post">' +
								'<input type="hidden" name="fountainId" id="fountainId" value="' + array[i]._id + '">' +
								'<input type="hidden" name="fountainPayRate" id="fountainPayRate" value="' + array[i].payRate + '">' +
								'<input type="text" name="sendToAddress" id="sendToAddress" placeholder="BTC or Email Address">' +
								'<input type="submit" placeholder="sendBTC">' +
							'</form>' 
						);
					}
				}
				return res.send('<div>Sorry! You are not in range of a fountain</div>');
			});
		})
	}
);

app.post('/account', 
	isAuthenticated,
	function (req, res) {
		block_io.get_address_balance({'address': req.user.address}, function (error, response) {
			return res.send(
				'<div>To fund your account send BTC to ' + req.user.address + '</div>' +
				'<div>Your account balance is ' + response.data.available_balance + '</div>'
			)
		})
	}
)

app.post('/sendBTC',
	function (req, res) {
		DbConnection().then(db => {
			db.collection('fountains').findOne({'_id': ObjectId(req.body.fountainId)}, function (err, fountain) {
				block_io.withdraw_from_addresses(
					{
						'amounts': req.body.fountainPayRate,
						'from_addresses': fountain.user.address,
						'to_addresses': req.body.sendToAddress
					}, 
					res.redirect('/')
				);
			});
		})
	}
);

io.on('connection', function (socket) {
	socket.on('getFountains', function (callback) {
		DbConnection().then(db => {
			db.collection('fountains').find().toArray(function (err, array) {
				callback(err, array)
			});
		})
	});
})